const puppeteer = require('puppeteer'); // подключаем библиотеку

const width = [1920, 1680, 1024, 920, 768, 568, 480, 320];
let name = process.argv[2] || 'https://yandex.ru/';
let time = process.argv[3] || 10000;
console.log(name);
console.log(time);

let colIter = 1;

(async () => {
    const browser = await puppeteer.launch();
    // const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage(); // создаем новую вкладку
    let flag = false;

    await page.goto(name, { waitUntil: 'networkidle0' }); // переходим по ссылке

    await page.setViewport({ width: 1920, height: 0 });
    // await page.waitForSelector('img');

    await page.waitForTimeout(3000);

    await page.evaluate(() => {

        // const el = document.getElementById('footer');
        // const el = document.querySelector('.pay_system_icons')
        // window.scrollTo(0, document.body.scrollHeight)
        // el.scrollIntoView({behavior: "smooth"});

        const elHeight = document.documentElement.scrollHeight;
        console.log('height: ' + document.documentElement.scrollHeight);
        let intH = 400;
        colIter = Math.ceil(elHeight / 400);
        colIter = colIter * 1000;
        let flag = false;

        const intervalId = setInterval(function() {
            console.log('intH: ' + intH);
            window.scrollTo(0, intH);
            intH = intH + 400;
            if (intH > elHeight) {
                clearInterval(intervalId);
                console.log('Прокрутка завершена');
                window.scrollTo(0, 0);
                flag = true;
            }
        }, 1000)

    });

    await page.waitForTimeout(time);

    for (let item of width) {
        await page.setViewport({ width: item, height: 0 });
        await page.waitForTimeout(2000);
        await page.screenshot({ path: name.replace(/(?:\:\/\/|\/)/g, '_') + '_' + item + '.png', fullPage: true });
        await page.waitForTimeout(1000);
    }

    console.log('Скриншёт сделан!');

    await browser.close();    

})();
